import {
  UserOutlined,
  MailOutlined,
  LockOutlined,
  EyeTwoTone,
  EyeInvisibleOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  Typography,
} from "antd";
import axios from "axios";
import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";

import Logo from "../Logo/Logo";
import { RegisterImage } from "../../assets";

const { Title, Paragraph } = Typography;

export const RegisterComponent = () => {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { success, error } = Modal;

  const onFinish = (values) => {
    try {
      values = { ...values, role: "customer" };
      axios
        .post(`https://backend-tecnoverse.vercel.app/user/register`, values)
        .then((res) => {
          success({
            title: "Register Berhasil!",
            content: "Klik Ok untuk login dengan akun Anda!",
            onOk() {
              navigate("/login");
            },
          });
          setLoading(false);
        })
        .catch((err) => {
          error({
            title: "Register Gagal!",
            content: err.message,
          });
          setLoading(false);
        });
      setLoading(true);
    } catch (err) {
      error({
        title: "Register Gagal!",
        content: err.message,
      });
      setLoading(false);
    }
  };

  return (
    <>
      <Row justify="space-around" style={{ marginTop: 100 }} align="middle">
        <Col xl={9} lg={10} md={10} sm={12}>
          <img
            src={RegisterImage}
            alt=""
            style={{
              width: "100%",
            }}
          />
        </Col>
        <Col xl={8} lg={10} md={10} sm={12}>
          <Card
            bodyStyle={{
              boxShadow: "0px 0px 47px 0px rgba(0,0,0,0.1)",
              marginBottom: "20%",
            }}
            bordered={false}
          >
            <Link to="/" className="menuItem">
              <Logo />
            </Link>
            <Title level={3}>Daftar Sekarang</Title>
            <Paragraph>
              Sudah punya akun? <Link to="/login">Masuk</Link>
            </Paragraph>
            <Divider />
            <Form layout="vertical" autoComplete="off" onFinish={onFinish}>
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Username required!",
                  },
                  { whitespace: true },
                  { min: 4 },
                ]}
                hasFeedback
                label="Username"
              >
                <Input
                  allowClear
                  size="large"
                  placeholder="Type your Username!"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                />
              </Form.Item>
              <Form.Item
                name="email"
                rules={[
                  {
                    required: true,
                    message: "Email required!",
                  },
                  {
                    type: "email",
                    message: "Enter valid email!",
                  },
                ]}
                hasFeedback
                label="Email"
              >
                <Input
                  allowClear
                  size="large"
                  placeholder="Type your Email!"
                  type="email"
                  prefix={<MailOutlined className="site-form-item-icon" />}
                />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Password required!",
                  },
                  {
                    validator: (_, value) =>
                      value &&
                      value.match(
                        "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$$"
                      )
                        ? Promise.resolve()
                        : Promise.reject(
                            "Password has minimum 8 characters, and contain at least one uppercase, lowercase, number, and special character!"
                          ),
                  },
                ]}
                hasFeedback
                label="Password"
              >
                <Input.Password
                  allowClear
                  size="large"
                  placeholder="Type your Password!"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                />
              </Form.Item>
              <Form.Item
                name="confirmPassword"
                dependencies={["password"]}
                rules={[
                  {
                    required: true,
                    message: "Confirm Password required!",
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      let matchValidator = value.match(
                        "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$$"
                      );
                      if (matchValidator) {
                        return Promise.resolve();
                      }
                      return Promise.reject("Password not match!");
                    },
                  }),
                ]}
                hasFeedback
                label="Confirm Password"
              >
                <Input.Password
                  allowClear
                  size="large"
                  placeholder="Retype your Password!"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                  block
                  size="large"
                  loading={loading}
                >
                  Daftar
                </Button>
              </Form.Item>
              <Paragraph style={{ color: "#bebebe" }}>
                Dengan mendaftar, saya menyetujui Syarat dan Ketentuan serta
                Kebijakan Privasi
              </Paragraph>
            </Form>
          </Card>
        </Col>
      </Row>
    </>
  );
};
