import { Input, Button, Badge } from "antd";
import {
  UserOutlined,
  LogoutOutlined,
  DashboardOutlined,
} from "@ant-design/icons";
import { Dropdown } from "antd";
import { Link, useNavigate, useSearchParams } from "react-router-dom";
import Logo from "../Logo/Logo";
import "./Menu.css";
import { UserContext } from "../../context/User.context";
import { useContext, useState } from "react";

const { Search } = Input;

const Menu = ({ filterLength }) => {
  let navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const { user, setUser } = useContext(UserContext);
  const [searchParams] = useSearchParams();

  const onSearch = (value) => {
    searchParams.set("keyword", value);

    navigate({
      pathname: "products",
      search: searchParams.toString(),
    });
  };

  const logout = () => {
    setLoading(true);
    setTimeout(() => {
      if (loading === false) {
        setUser(null);
        localStorage.clear();
        navigate("/login");
      }
    }, user);
  };

  const items = [
    {
      key: "1",
      label: (
        <div style={{ textAlign: "center" }}>
          <a
            style={{ color: "black" }}
            onClick={() => {
              navigate("/user");
            }}
          >
            <DashboardOutlined style={{ fontSize: 15, marginRight: 5 }} />
            Dashboard
          </a>
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <Button
          onClick={() => {
            logout();
          }}
          icon={<LogoutOutlined />}
          loading={loading}
          type="primary"
          size="middle"
          danger
          style={{ width: "100%" }}
        >
          Logout
        </Button>
      ),
      disabled: true,
    },
  ];
  return (
    <div className="menu">
      <Link to="/" className="menuItem">
        <Logo />
      </Link>

      {user ? (
        <>
          <Dropdown
            menu={{
              items,
            }}
            placement="bottom"
            arrow
          >
            <Button icon={<UserOutlined style={{ fontSize: 22 }} />} />
          </Dropdown>
        </>
      ) : (
        <>
          <div className="btn-menu">
            <Button>
              <Link to="login">Login</Link>
            </Button>
            <Button type="primary">
              <Link to="register">Register</Link>
            </Button>
          </div>
        </>
      )}
    </div>
  );
};

export default Menu;
