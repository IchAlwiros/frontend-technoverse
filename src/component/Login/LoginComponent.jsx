import {
  EyeInvisibleOutlined,
  EyeTwoTone,
  LockOutlined,
  UserOutlined,
} from "@ant-design/icons";
import {
  Button,
  Card,
  Col,
  Divider,
  Form,
  Input,
  Modal,
  Row,
  Typography,
} from "antd";
import { LoginImage } from "../../assets";
import { useContext, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import Logo from "../Logo/Logo";
import { UserContext } from "../../context/User.context";
import axios from "axios";

const { Title, Paragraph } = Typography;

export const LoginComponent = () => {
  const { setUser } = useContext(UserContext);
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const { success, error } = Modal;

  const validateEmail = (email) => {
    const regexExp =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gi;
    return regexExp.test(email);
  };

  const onFinish = (values) => {
    if (validateEmail(values.usernameEmail)) {
      values = { ...values, email: values.usernameEmail };
      delete values.usernameEmail;
    } else {
      values = { ...values, username: values.usernameEmail };
      delete values.usernameEmail;
    }
    try {
      axios
        .post(`https://backend-tecnoverse.vercel.app/user/login`, values)
        .then((res) => {
          setUser(res.data.data);
          localStorage.setItem("user", JSON.stringify(res.data.data));

          let content = "Selamat beraktivitas!";

          success({
            title: "Login Berhasil!",
            content: content,
          });
          setLoading(false);
          navigate("/user");
        })
        .catch((err) => {
          error({
            title: "Login Gagal!",
            content: "Please insert valid credentials",
          });
          setLoading(false);
        });

      setLoading(true);
    } catch (err) {
      error({
        title: "Login Gagal!",
        content: err.message,
      });
      setLoading(false);
    }
  };

  return (
    <>
      <Row justify="space-around" style={{ marginTop: 80 }} align="middle">
        <Col xl={8} lg={10} md={10} sm={12}>
          <Card
            bodyStyle={{
              boxShadow: "0px 0px 47px 0px rgba(0,0,0,0.1)",
            }}
            bordered={false}
          >
            <Link to="/" className="menuItem">
              <Logo />
            </Link>
            <Title level={3}>Masuk ke Akun Anda!</Title>
            <Paragraph>
              Belum punya akun?
              <Link to="/register"> Register</Link>
            </Paragraph>
            <Divider />
            <Form layout="vertical" autoComplete="off" onFinish={onFinish}>
              <Form.Item
                label="Username or Email"
                hasFeedback
                name="usernameEmail"
                rules={[
                  {
                    required: true,
                    message: "Username or Email required!",
                  },
                  { whitespace: true },
                  { min: 4 },
                ]}
              >
                <Input
                  allowClear
                  size="large"
                  placeholder="Type Your Username or Email!"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                />
              </Form.Item>
              <Form.Item
                label="Password"
                name="password"
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Password required!",
                  },
                  {
                    min: 6,
                  },
                ]}
              >
                <Input.Password
                  allowClear
                  size="large"
                  placeholder="Type Your Password!"
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  iconRender={(visible) =>
                    visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
                  }
                />
              </Form.Item>
              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                  size="large"
                  block
                  loading={loading}
                >
                  Masuk
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
        <Col xl={9} lg={10} md={10} sm={12}>
          <img
            src={LoginImage}
            alt=""
            style={{
              width: "90%",
            }}
          />
        </Col>
      </Row>
    </>
  );
};
