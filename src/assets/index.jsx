import LoginImage from "./img/login.svg";
import RegisterImage from "./img/register.svg";
import HomeImage from "./img/home.svg";

export { LoginImage, RegisterImage, HomeImage };
