import { Layout, theme } from "antd";
const { Footer } = Layout;

export const Footers = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Footer
      style={{
        textAlign: "center",
        // marginTop: 50,
        background: colorBgContainer,
        color: "black",
        boxShadow: " 0px -10px 5px rgba(0,0,0,0.1)",
      }}
    >
      <strong>Technoverse ©2023 Created by Ichal</strong>
    </Footer>
  );
};
