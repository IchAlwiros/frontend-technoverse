import { Divider, Button } from "antd";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { HomeImage } from "../../../assets";
import { UserContext } from "../../../context/User.context";

export const Home = () => {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  return (
    <div style={{ minHeight: "80.5vh", padding: 20 }}>
      <Divider />
      <div style={{ display: "flex", justifyContent: "space-evenly" }}>
        <div style={{ marginTop: "8%" }}>
          <span style={{ fontSize: 25, fontWeight: "bold" }}>
            Easy to Manage
          </span>
          <br />
          <span style={{ fontSize: 18, fontWeight: "normal" }}>
            Product Maintain
          </span>
          <br />
          <span>
            Managing your product becomes easier
            <br />
            Technology is a form of development. make
            <br />
            someone not stop learning to keep growing
          </span>
          <br />
          <Button
            style={{
              background: "#0A4D68",
              color: "white",
              marginTop: 10,
              height: 40,
              width: 200,
            }}
            onClick={() => {
              user ? navigate("/user") : navigate("/login");
            }}
          >
            Get Started
          </Button>
        </div>
        <div>
          <img
            alt={1}
            src={HomeImage}
            style={{ width: "500px", height: "300px", objectFit: "cover" }}
          />
        </div>
      </div>
    </div>
  );
};
