import { Breadcrumb, Layout } from "antd";
import { useNavigate } from "react-router-dom";
import { LoginComponent } from "../../../component/index";

export const Login = () => {
  let navigate = useNavigate();
  return (
    <>
      <div
        className="wrapper"
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "space-evenly",
        }}
      >
        <div className="navbar">
          <Breadcrumb style={{ margin: "30px 0 0 100px" }}>
            <Breadcrumb.Item>
              <a
                onClick={() => {
                  navigate("/");
                }}
              >
                Home
              </a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Login</Breadcrumb.Item>
          </Breadcrumb>

          <Layout style={{ background: "white" }}>
            <div className="login-area">
              <LoginComponent />
            </div>
          </Layout>
        </div>

        <div className="footers">{/* <Footers /> */}</div>
      </div>
    </>
  );
};
