import { Layout, theme } from "antd";
import Menu from "../component/Menu/Menu";

const { Header } = Layout;

const CustomHeader = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  return (
    <>
      <Header
        style={{
          position: "sticky",
          top: 0,
          zIndex: 1,
          width: "100%",
          background: colorBgContainer,
          height: "auto",
          boxShadow: "0px 10px 15px -3px rgba(0,0,0,0.1)",
        }}
      >
        <Menu />
      </Header>
    </>
  );
};

export default CustomHeader;
