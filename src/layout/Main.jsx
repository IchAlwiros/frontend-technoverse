import { Col, Layout, Row } from "antd";
import { Outlet } from "react-router-dom";
import CustomHeader from "./Header";
import { Footers } from "./index";

const { Content } = Layout;

export const Main = () => {
  return (
    <>
      <div
        className="wrapper"
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "space-between",
        }}
      >
        <div className="navbar">
          <CustomHeader />
          <Layout>
            <Row justify="center" style={{ background: "white" }}>
              <Col span={18}>
                <Content style={{ padding: "0 50px" }}>
                  <div
                    className="site-layout-content"
                    style={{ marginTop: "10px" }}
                  >
                    <Outlet />
                  </div>
                </Content>
              </Col>
            </Row>
          </Layout>
        </div>

        <div className="footers">
          <Footers />
        </div>
      </div>
    </>
  );
};
