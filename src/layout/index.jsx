// IMPORT || Auth Login & Register
export { Login } from "./landingpage/login/Login";
export { Register } from "./landingpage/register/Register";
export { Main } from "./Main";
export { Home } from "./landingpage/home/Home";
export { Footers } from "./Footers";
export { DashboardUser } from "./admin/dashboard/DashboardUser";

export { ProductTable } from "./admin/product/Table";
export { ProductForm } from "./admin/product/Form";
export { ScanProduct } from "./admin/scan/ScanProduct";

export { ChangePassword } from "./admin/profile/ChangePwd";
export { ProfileEdit } from "./admin/profile/ProfileEdit";
export { ProfileUser } from "./admin/profile/ProfileUser";
