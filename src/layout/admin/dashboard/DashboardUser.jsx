import React from "react";
import { Layout } from "antd";
import { createContext, useState } from "react";
import { Outlet } from "react-router-dom";
import CustomHeader from "./Header";
import CustomSidebar from "./Sidebar";

import "./User.css";

const { Content } = Layout;

export const MainContext = createContext();

export const DashboardUser = () => {
  const [collapsed, setCollapsed] = useState(false);
  return (
    <MainContext.Provider value={{ collapsed, setCollapsed }}>
      <Layout hasSider>
        <CustomSidebar />
        <Layout>
          <CustomHeader />
          <Content
            style={{
              margin: "24px 16px 0",
              overflow: "initial",
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                minHeight: "100vh",
                justifyContent: "space-between",
              }}
            >
              <div
                className="content-section"
                style={{
                  background: "#fff",
                  margin: "0px auto",
                  padding: "20px",
                  width: "90%",
                }}
              >
                <Outlet />
              </div>
            </div>
          </Content>
        </Layout>
      </Layout>
    </MainContext.Provider>
  );
};
