import { Avatar, Layout, Dropdown, Button } from "antd";
import React, { useContext, useState } from "react";
import { MainContext } from "./DashboardUser";
import {
  LeftCircleOutlined,
  RightCircleOutlined,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../../context/User.context";

const { Header } = Layout;

const CustomHeader = () => {
  const { user, setUser } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  let { collapsed, setCollapsed } = useContext(MainContext);
  let navigate = useNavigate();
  const logout = () => {
    setLoading(true);
    setTimeout(() => {
      if (loading === false) {
        setUser(null);
        localStorage.clear();
        navigate("/login");
      }
    }, user);
  };

  const items = [
    {
      key: "logout",
      label: (
        <Button
          onClick={() => {
            logout();
          }}
          icon={<LogoutOutlined />}
          loading={loading}
          type="primary"
          size="middle"
          danger
          style={{ width: "100%" }}
        >
          Logout
        </Button>
      ),
    },
  ];

  return (
    <>
      <Header
        style={{
          padding: 0,
          background: "#fff",
          textAlign: "left",
          paddingLeft: "2rem",
          paddingRight: "2rem",
          display: "flex",
        }}
      >
        <div>
          {React.createElement(
            collapsed ? RightCircleOutlined : LeftCircleOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </div>
        <div
          style={{
            marginLeft: "auto",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            gap: "1rem",
          }}
        >
          <p>Welcome, {user?.username}</p>
          <Dropdown
            menu={{
              items,
            }}
            placement="bottom"
            arrow
          >
            <Avatar
              style={{
                backgroundColor: "#000",
              }}
              icon={<UserOutlined />}
            />
          </Dropdown>
        </div>
      </Header>
    </>
  );
};

export default CustomHeader;
