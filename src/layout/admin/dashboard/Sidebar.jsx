import {
  DashboardOutlined,
  ScanOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import { useContext } from "react";
import { MainContext } from "./DashboardUser";
import { Link } from "react-router-dom";
import Logo from "../../../component/Logo/Logo";

const { Sider } = Layout;

const Sidebar = () => {
  const { collapsed } = useContext(MainContext);

  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const items = [
    {
      label: <Link to="/user">Dashboard</Link>,
      key: "dashboard",
      icon: <DashboardOutlined />,
    },
    {
      label: <Link to="scan">Scan Product</Link>,
      key: "scan",
      icon: <ScanOutlined />,
    },
    {
      label: <Link to="profile">Profile User</Link>,
      key: "profile",
      icon: <UserOutlined />,
    },
  ];
  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={collapsed}
      style={{ background: colorBgContainer }}
    >
      <div style={{ height: 32, margin: 16, textAlign: "center" }}>
        <Link to="/" className="menuItem">
          <Logo />
        </Link>
      </div>

      <Menu
        theme="light"
        mode="inline"
        defaultSelectedKeys={["1"]}
        items={items}
      />
    </Sider>
  );
};

export default Sidebar;
