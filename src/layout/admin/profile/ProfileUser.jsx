import { Avatar, Button, Skeleton } from "antd";
import axios from "axios";
import { useCallback, useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../../context/User.context";

export const ProfileUser = () => {
  const { user } = useContext(UserContext);
  const [data, setData] = useState(null);
  const navigate = useNavigate();

  const fetchData = useCallback(async () => {
    let res = await axios.get(`https://backend-tecnoverse.vercel.app/profile`, {
      headers: { Authorization: "Bearer " + user.token },
    });

    const userData = {
      ...res.data?.data,
    };
    setData(userData);
  }, [user]);

  useEffect(() => {
    if (data === null) {
      fetchData();
    }
  }, [data, fetchData]);

  return (
    <>
      <div style={{ display: "flex", justifyContent: "space-evenly" }}>
        <div style={{ textAlign: "center" }}>
          <h1>ProfiledUser</h1>
          <Avatar
            size={150}
            style={{
              backgroundColor: "#2C74B3",
              color: "white",
              fontSize: 50,
              margin: "auto 0",
            }}
          >
            {user.username.substring(0, 1).toUpperCase()}
          </Avatar>
          <br />
          <br />
          <Button
            type="primary"
            onClick={() => {
              navigate("/user/change-pwd");
            }}
          >
            Ubah Kata Sandi
          </Button>
        </div>
        <div>
          <h1>Biodata</h1>
          <h1>{user.username}</h1>
          {data !== null ? (
            <>
              <table style={{ textAlign: "left" }}>
                <tbody>
                  <tr>
                    <th>Email</th>
                    <td>{user.email}</td>
                  </tr>
                  <tr>
                    <th>Fullname</th>
                    <td>{data?.fullname}</td>
                  </tr>
                  <tr>
                    <th>Address</th>
                    <td>{data?.address}</td>
                  </tr>
                  <tr>
                    <th>Birthdate</th>
                    <td>{data?.birthdate}</td>
                  </tr>
                </tbody>
              </table>
              <div style={{ marginTop: "16px" }}>
                <Button
                  type="primary"
                  onClick={() => {
                    navigate("/user/profile-edit");
                  }}
                >
                  Edit Profile
                </Button>
              </div>
            </>
          ) : (
            <>
              <>
                <Skeleton />
                <Button type="primary" disabled>
                  Loading..
                </Button>
              </>
            </>
          )}
        </div>
      </div>
    </>
  );
};
