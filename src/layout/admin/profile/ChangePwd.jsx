import {
  Avatar,
  Breadcrumb,
  Button,
  Divider,
  Form,
  Input,
  Modal,
  message,
} from "antd";
import { SettingFilled } from "@ant-design/icons";
import { useLocation, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import axios from "axios";
import { UserContext } from "../../../context/User.context";

export const ChangePassword = () => {
  const { user, setUser } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onFinish = (values) => {
    let { oldpassword, newpassword, confirmpassword } = values;
    setLoading(true);
    axios
      .post(
        `https://backend-tecnoverse.vercel.app/user/change-password`,
        {
          oldpassword,
          newpassword,
          confirmpassword,
        },
        { headers: { Authorization: "Bearer " + user.token } }
      )
      .then((res) => {
        setTimeout(() => {
          if (loading === false) {
            setUser(null);
            localStorage.clear();
            navigate("/login");
            message.success(
              location.pathname === "/user/profile/change-pwd"
                ? "Password berhasil dibuat"
                : "Password berhasil diubah silakan login kembali"
            );
          }
        }, res);
      })
      .catch((err) => {
        const data = err.response.data;
        let { oldpassword, newpassword } = data.error;

        Modal.error({
          title: "Error Change Password",
          content:
            oldpassword && newpassword
              ? `${oldpassword} & ${newpassword}, Please Try Again Change`
              : oldpassword
              ? `${oldpassword}, Please Try Again Change`
              : `${newpassword}, Please Try Again Change`,
        });
        setLoading(false);
      });
  };

  function ChangePaswword() {
    return (
      <div style={{ margin: 100 }}>
        <h1>Change Password</h1>
        <br></br>

        <strong> Old Password </strong>
        <label style={{ color: "red" }}> * </label>
        <Form
          name="basic"
          labelCol={{ span: 5 }}
          wrapperCol={{ span: "100%" }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            name="oldpassword"
            rules={[
              {
                required: true,
                message: "Please input your correct password! min 6 character",
              },
            ]}
          >
            <Input.Password placeholder="Your Old Password" />
          </Form.Item>

          <strong> New Password </strong>
          <label style={{ color: "red" }}> * </label>
          <Form.Item
            name="newpassword"
            rules={[
              {
                min: 6,
                required: true,
                message: "Please input your correct password! min 6 character",
              },
            ]}
          >
            <Input.Password placeholder="Your new password" />
          </Form.Item>

          <strong> Confirm Password </strong>
          <label style={{ color: "red" }}> * </label>
          <Form.Item
            name="confirmpassword"
            rules={[
              {
                min: 6,
                required: true,
                message: "Please input your correct password! min 6 character",
              },
            ]}
          >
            <Input.Password placeholder="Confirm Your Password" />
          </Form.Item>
          <Divider
            style={{ background: "whitesmoke", margin: "10px 0 20px 0" }}
          />
          <Button
            type="primary"
            block
            loading={loading}
            htmlType="submit"
            style={{
              borderRadius: "5px",
              height: 40,
              width: "30%",
            }}
          >
            Change Password
          </Button>
        </Form>
      </div>
    );
  }

  return (
    <div>
      <Breadcrumb style={{ margin: "30px 0 0 100px" }}>
        <Breadcrumb.Item>
          <a
            onClick={() => {
              navigate("/user/profile");
            }}
          >
            Profile
          </a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>Change Password</Breadcrumb.Item>
      </Breadcrumb>

      <div
        style={{
          background: "white",
          margin: "31px 75px 30px 75px",
          height: "100%",
          padding: "10px",
          boxShadow:
            "rgba(50, 50, 93, 0.25) 0px 30px 60px -12px, rgba(0, 0, 0, 0.3) 0px 18px 36px -18px",
          borderRadius: 7,
        }}
      >
        <ChangePaswword />
      </div>
    </div>
  );
};
