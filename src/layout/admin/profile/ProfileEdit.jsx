import {
  Breadcrumb,
  Button,
  DatePicker,
  Divider,
  Form,
  Input,
  Row,
  Col,
} from "antd";
import { SettingFilled } from "@ant-design/icons";
import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../../context/User.context";

export const ProfileEdit = () => {
  let navigate = useNavigate();
  let profileObj = {
    address: "",
    birthdate: "",
    fullname: "",
  };
  const [profile, setProfile] = useState(null);
  const [input, setInput] = useState(profileObj);
  const [loading, setLoading] = useState(false);
  const { user } = useContext(UserContext);
  const [form] = Form.useForm();

  const fetchData = async () => {
    setLoading(true);
    await axios
      .get(`https://backend-tecnoverse.vercel.app/profile`, {
        headers: { Authorization: "Bearer " + user.token },
      })
      .then((res) => {
        setLoading(false);
        const profileData = {
          ...res.data.data,
        };
        setProfile(profileData);
        console.log(profileData);
        form.setFieldsValue({
          // birthdate: profileData.birthdate,
          fullname: profileData.fullname,
          address: profileData.address,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    if (profile === null) {
      fetchData();
    }
  }, [profile, form]);

  const onFinish = (values) => {
    let { birthdate, fullname, address } = values;
    // console.log(birthdate.toISOString().slice(0, 10));
    setLoading(true);
    axios
      .patch(
        `https://backend-tecnoverse.vercel.app/profile`,
        {
          birthdate: birthdate.toISOString().slice(0, 10),
          fullname,
          address,
        },
        { headers: { Authorization: "Bearer " + user.token } }
      )
      .then((res) => {
        navigate("/user/profile");
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  function FormEdit() {
    return (
      <div
        style={{
          background: "white",
          margin: " 10% 10% 10% 10%",
          padding: 30,
          borderRadius: 20,
          textAlign: "left",
          boxShadow:
            "rgba(50, 50, 93, 0.25) 0px 30px 60px -12px, rgba(0, 0, 0, 0.3) 0px 18px 36px -18px",
        }}
      >
        {input?.fullname}
        <h1>Edit Profile</h1>
        <br></br>
        <Row justify="left">
          <Col xs={24} sm={22} md={18} lg={14}>
            <Form
              layout="vertical"
              form={form}
              onFinish={onFinish}
              autoComplete="off"
              style={{ textAlign: "left" }}
            >
              <Form.Item
                label="Birthdate"
                name={"birthdate"}
                rules={[
                  {
                    required: true,
                    message: "Input Your Birthdate!",
                  },
                ]}
              >
                <DatePicker />
              </Form.Item>
              <Form.Item
                label="Fullname"
                name={"fullname"}
                rules={[
                  {
                    type: "string",
                    required: true,
                    message: "Input Your Last Name!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Address"
                name={"address"}
                rules={[
                  {
                    type: "string",
                    required: true,
                    message: "Input Your First Name!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Divider />
              <Button
                loading={loading}
                type="primary"
                htmlType="submit"
                disabled={loading}
                style={{
                  height: 40,
                  borderRadius: 5,
                  borderColor: "#088395",
                }}
              >
                Update
              </Button>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }

  return (
    <>
      <div>
        <Breadcrumb style={{ margin: "30px 0 0 100px" }}>
          <Breadcrumb.Item>
            <a
              onClick={() => {
                navigate("/user/profile");
              }}
            >
              Profile
            </a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Profile Edit</Breadcrumb.Item>
        </Breadcrumb>
        <FormEdit />
      </div>
    </>
  );
};
