import axios from "axios";
import { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Button, Form, Input } from "antd";
import Barcode from "react-barcode";
import { Content } from "antd/lib/layout/layout";
import { UserContext } from "../../../context/User.context";
import { ShoppingCartOutlined, UserOutlined } from "@ant-design/icons";
import "./DetailProduct.css";

export const ScanProduct = () => {
  const [product, setProduct] = useState({});
  const [fetchDataLoading, setFetchDataLoading] = useState(false);
  const [value, setValue] = useState();

  const { user } = useContext(UserContext);
  let navigate = useNavigate();

  useEffect(() => {
    console.log(value);
    setProduct(product);
    if (value) {
    }
  }, [product]);

  const formatter = new Intl.NumberFormat(
    ("id-ID",
    {
      style: "currency",
      currency: "IDR",
    })
  );

  const onFinish = (values) => {
    setValue(values);
    setFetchDataLoading(true);
    axios
      .post(`https://backend-tecnoverse.vercel.app/product/scan`, values, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then((res) => {
        const { data } = res.data;

        setProduct(data);
        setFetchDataLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setFetchDataLoading(false);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  function FormInput() {
    return (
      <Form
        layout="vertical"
        autoComplete="off"
        style={{ textAlign: "center" }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Code Number"
          name="code"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" loading={fetchDataLoading}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }

  return (
    <>
      <div
        className="wrapper"
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "space-between",
        }}
      >
        <div className="navbar">
          <Content
            style={{
              display: "inline-flex",
              justifyContent: "space-around",
              padding: 10,
              width: "100%",
              margin: "10px 0",
            }}
          >
            <div className="foto-product">
              <div
                style={{
                  marginBottom: 20,
                  background: "white",
                  padding: 10,
                  borderRadius: 10,
                }}
              >
                <h2>Barcode</h2>
                <Barcode value={product?.code} />
              </div>
              <div
                style={{
                  overflow: "auto",
                  display: "flex",
                  justifyContent: "center",
                  background: "white",
                  textAlign: "center",
                  width: 270,
                }}
              >
                <FormInput />
              </div>
            </div>
            <div className="info-product">
              <>
                <div
                  style={{
                    marginBottom: "15px",
                    display: "flex",
                    flexDirection: "row",
                    gap: "20px",
                    alignItems: "center",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      gap: "10px",
                    }}
                  >
                    <div
                      style={{
                        cursor: "pointer",
                        fontSize: "16px",
                        fontWeight: "bold",
                      }}
                      onClick={() => navigate(``)}
                    ></div>
                  </div>
                </div>

                <h2>{product?.name}</h2>
                <hr></hr>

                <div
                  style={{
                    fontWeight: "600",
                    fontSize: "18px",
                    margin: "10px 0px 25px",
                  }}
                >
                  Price:{" "}
                  {product?.price ? formatter.format(product?.price) : "0"}
                </div>
                <table style={{ textAlign: "left" }}>
                  <tbody>
                    <tr>
                      <th>Code Number</th>
                      <td>{product?.code}</td>
                    </tr>
                    <tr>
                      <th>Description</th>
                      <td>{product?.description}</td>
                    </tr>
                  </tbody>
                </table>
              </>
              <br></br>
            </div>
          </Content>
        </div>

        <div className="footers">{/* <Footers /> */}</div>
      </div>
    </>
  );
};
