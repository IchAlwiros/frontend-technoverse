import { useContext, useEffect, useState } from "react";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import axios from "axios";

import { UserContext } from "../../../context/User.context";

import {
  Button,
  Col,
  Form,
  Input,
  InputNumber,
  message,
  Row,
  Space,
  Typography,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { generateBarcode } from "../../../utils/GenerateCode";
import { RedoOutlined } from "@ant-design/icons";
const { Title } = Typography;

export const ProductForm = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { id } = useParams();

  const { user } = useContext(UserContext);
  const [form] = Form.useForm();

  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (id) {
      axios
        .get(`https://backend-tecnoverse.vercel.app/product/${id}`, {
          headers: { Authorization: `Bearer ${user.token}` },
        })
        .then((res) => {
          const { data } = res.data;

          console.log(data.code);
          form.setFieldsValue({
            name: data.name,
            description: data.description,
            price: data.price,
            code: data.code,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [user, id, form]);

  const onFinish = async (value) => {
    setLoading(true);
    let productId = 0;
    try {
      if (id) {
        const res = await axios.patch(
          `https://backend-tecnoverse.vercel.app/product/${id}`,
          {
            name: value.name,
            description: value.description,
            price: value.price,
            code: value.code,
          },
          { headers: { Authorization: `Bearer ${user.token}` } }
        );

        productId = res.data.data.id;
      } else {
        const res = await axios.post(
          `https://backend-tecnoverse.vercel.app/product`,
          {
            name: value.name,
            description: value.description,
            price: value.price,
            code: value.code,
          },
          { headers: { Authorization: `Bearer ${user.token}` } }
        );

        productId = res.data.data.id;
      }

      message.success(
        location.pathname === "/user/product/create"
          ? "Produk berhasil ditambahkan"
          : "Produk berhasil diedit"
      );

      setLoading(false);
      navigate("/user");
    } catch (err) {
      console.log(err);

      message.error(
        location.pathname === "/user/product/create"
          ? "Gagal menambahkan produk. Silahkan coba kembali."
          : "Gagal mengedit produk. Silahkan coba kembali."
      );

      setLoading(false);
    }
  };

  const GenerateCode = () => {
    let str = "RandomCode";
    form.setFieldsValue({
      code: generateBarcode(str),
    });
  };

  return (
    <Row justify="center">
      <Col xs={24} sm={22} md={18} lg={14}>
        <div
          style={{
            marginBottom: "30px",
            borderBottom: "2px solid #088395",
            padding: "10px 15px 0px",
            textAlign: "center",
          }}
        >
          <Title level={4}>
            {location.pathname === "/user/product/create"
              ? "ADD NEW PRODUCT"
              : "EDIT PRODUCT DATA"}
          </Title>
        </div>

        <Form
          form={form}
          layout="vertical"
          onFinish={onFinish}
          autoComplete="off"
          style={{ textAlign: "left" }}
        >
          <Form.Item
            name="name"
            label="Nama produk"
            rules={[
              {
                required: true,
              },
              {
                type: "string",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="description"
            label="Deskripsi Produk"
            rules={[
              {
                required: true,
              },
              {
                type: "string",
              },
            ]}
          >
            <TextArea rows={4} />
          </Form.Item>
          <Form.Item
            name="code"
            label="Code Produk"
            rules={[
              {
                required: true,
              },
              {
                type: "string",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Button
            icon={<RedoOutlined />}
            onClick={() => {
              GenerateCode();
            }}
          >
            Generate code
          </Button>
          <Space size="middle" style={{ width: "100%", marginTop: "5%" }}>
            <Form.Item
              name="price"
              label="Harga"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <InputNumber addonBefore="Rp" min={0} />
            </Form.Item>
          </Space>

          {/* Kategori Input */}

          <Form.Item style={{ marginTop: "40px", textAlign: "center" }}>
            <Space size="middle">
              <Button htmlType="button" onClick={() => navigate("/user")}>
                Cancel
              </Button>
              <Button type="primary" htmlType="submit" loading={loading}>
                Submit
              </Button>
            </Space>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};
