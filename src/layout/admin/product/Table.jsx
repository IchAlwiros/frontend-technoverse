import { useContext, useEffect, useState } from "react";
import axios from "axios";

import { UserContext } from "../../../context/User.context";

import { Button, Image, message, Popconfirm, Space, Table, Tag } from "antd";
import { EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import Barcode from "react-barcode";

export const ProductTable = () => {
  const navigate = useNavigate();
  const { user } = useContext(UserContext);

  const [dataSource, setDataSource] = useState([]);
  const [loading, setLoading] = useState(false);
  const [deletedId, setDeletedId] = useState(0);
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    axios
      .get(`https://backend-tecnoverse.vercel.app/product`, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then((res) => {
        const { data } = res.data;

        const filterdata = data.filter((el) => el.isDeleted !== true);

        const structuredData = filterdata.map(
          ({ id, name, description, code, price, isDeleted }) => ({
            key: id,
            product: {
              name,
            },
            code,
            price,
            expandableData: {
              description,
            },
          })
        );

        setDataSource(structuredData);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }, [user]);

  const handleEdit = (id) => {
    navigate(`/user/edit/${id}`);
  };

  const confirm = () => {
    setConfirmLoading(true);
    axios
      .delete(`https://backend-tecnoverse.vercel.app/product/${deletedId}`, {
        headers: { Authorization: `Bearer ${user.token}` },
      })
      .then((res) => {
        const newDataSource = dataSource.filter((el) => el.key !== deletedId);
        setDataSource(newDataSource);

        setConfirmLoading(false);
        setDeletedId(0);
        setOpen(false);

        message.success(res.data.message);
      })
      .catch((err) => {
        setConfirmLoading(false);
        setDeletedId(0);
        setOpen(false);

        message.error(err.response.data.message);
      });
  };

  const cancel = () => {
    setOpen(false);
    setDeletedId(0);
  };

  const columns = [
    {
      title: "Produk",
      dataIndex: "product",
      key: "product",
      render: (_, { product }) => (
        <Space>
          <Image width={90} src={product.coverImage} />
          <p>{product.name}</p>
        </Space>
      ),
    },
    {
      title: "Harga",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Code",
      dataIndex: "code",
      key: "code",
    },

    {
      title: "Aksi",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            icon={<EditOutlined />}
            onClick={() => {
              handleEdit(record.key);
            }}
          />
          <Popconfirm
            placement="topRight"
            title="Hapus Produk"
            description="Apakah kamu yakin ingin menghapus produk ini?"
            open={() => {
              if (deletedId) {
                if (record.key === deletedId) {
                  return true;
                } else {
                  return false;
                }
              } else {
                return open;
              }
            }}
            onConfirm={confirm}
            onCancel={cancel}
            okText="Ya"
            cancelText="Tidak"
            okButtonProps={{
              loading: confirmLoading,
            }}
          >
            <Button
              danger
              icon={
                <DeleteOutlined
                  onClick={() => {
                    setDeletedId(record.key);
                    setOpen(true);
                  }}
                />
              }
            />
          </Popconfirm>
        </Space>
      ),
    },
  ];

  return (
    <>
      <div
        style={{
          marginBottom: "15px",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <div
          style={{
            paddingBottom: "5px",
            borderBottom: "2px solid #088395",
            fontWeight: "600",
            fontSize: "18px",
          }}
        >
          DATA PRODUK
        </div>
        <Button
          type="primary"
          onClick={() => {
            navigate("/user/product/create");
          }}
        >
          Tambah Produk
        </Button>
      </div>
      <Table
        columns={columns}
        expandable={{
          expandedRowRender: (record) => (
            <Space direction="vertical" size="middle">
              <Space size="middle">
                {
                  <>
                    <Barcode value={record.code} />
                  </>
                }
              </Space>
              <Space direction="vertical">
                <div
                  style={{
                    fontWeight: "bold",
                    borderBottom: "2px solid #088395",
                    paddingBottom: "5px",
                    width: "fit-content",
                  }}
                >
                  Deskripsi Produk
                </div>
                <p
                  style={{
                    margin: 0,
                  }}
                >
                  {record.expandableData.description}
                </p>
              </Space>
            </Space>
          ),
        }}
        dataSource={dataSource}
        pagination={false}
        loading={loading}
      />
    </>
  );
};
