import { createContext, useState } from "react";

export const UserContext = createContext();

export const UserProvider = (props) => {
  let [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  return (
    <UserContext.Provider value={{ user, setUser }}>
      {props.children}
    </UserContext.Provider>
  );
};
