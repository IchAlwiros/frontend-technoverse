import { ConfigProvider } from "antd";
import "./App.css";
import MainRoutes from "./router/route";
import { theme } from "./theme/Theme";
import { UserProvider } from "./context/User.context";

function App() {
  return (
    <div className="App">
      <UserProvider>
        <ConfigProvider theme={theme}>
          <MainRoutes />
        </ConfigProvider>
      </UserProvider>
    </div>
  );
}

export default App;
