import { useContext } from "react";
import { BrowserRouter, Routes, Route, Navigate, Link } from "react-router-dom";
// import { ChangePassword, ProfileEdit, ProfileUser } from "../components";
import { UserContext } from "../context/User.context";
import {
  Login,
  Register,
  Main,
  Home,
  DashboardUser,
  ProductTable,
  ScanProduct,
  ProductForm,
  ProfileUser,
  ChangePassword,
  ProfileEdit,
} from "../layout/index";

const MainRouter = () => {
  const { user } = useContext(UserContext);
  const PrivateRoute = ({ children }) => {
    // Handle Back To Login
    if (user) {
      return children;
    } else {
      return <Navigate to="/login" replace={true} />;
    }
  };

  const LoginRoute = ({ children }) => {
    if (user) {
      return <Navigate to="/" replace={true} />;
    } else {
      return children;
    }
  };
  return (
    <>
      <BrowserRouter>
        <Routes>
          {/* Auth */}
          <Route
            path="/login"
            element={
              <LoginRoute>
                <Login />
              </LoginRoute>
            }
          />

          <Route
            path="/register"
            element={
              <LoginRoute>
                <Register />
              </LoginRoute>
            }
          />

          <Route path="/" element={<Main />}>
            <Route index element={<Home />} />
          </Route>

          <Route path="/user" element={<DashboardUser />}>
            <Route
              path="profile"
              element={
                <PrivateRoute>
                  <ProfileUser />
                </PrivateRoute>
              }
            />

            <Route
              path="change-pwd"
              element={
                <PrivateRoute>
                  <ChangePassword />
                </PrivateRoute>
              }
            />

            <Route
              path="profile-edit"
              element={
                <PrivateRoute>
                  <ProfileEdit />
                </PrivateRoute>
              }
            />
            <Route
              index
              element={
                <PrivateRoute>
                  <ProductTable />
                </PrivateRoute>
              }
            />
            <Route
              path="edit/:id"
              element={
                <PrivateRoute>
                  <ProductForm />
                </PrivateRoute>
              }
            />
            <Route
              path="product/create"
              element={
                <PrivateRoute>
                  <ProductForm />
                </PrivateRoute>
              }
            />
            <Route
              path="scan"
              element={
                <PrivateRoute>
                  <ScanProduct />
                </PrivateRoute>
              }
            />
          </Route>

          <Route path="*" element={<NoMatchPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

function NoMatchPage() {
  return (
    <div>
      <h2>Nothing to see here!</h2>
      <p>
        <Link to="/login">Back to login</Link>
      </p>
    </div>
  );
}

export default MainRouter;
