export function generateBarcode(str) {
  // Menghasilkan 5 huruf acak
  let letters = "";
  const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (let i = 0; i < 5; i++) {
    letters += alphabet.charAt(Math.floor(Math.random() * alphabet.length));
  }

  // Menghasilkan 3 angka acak
  const numbers = Math.floor(Math.random() * 900) + 100;

  // Menggabungkan huruf dan angka
  const barcode = letters + numbers;

  return barcode;
}
